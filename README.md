TESTAR Docker - DECODER version
===============================

TESTAR Docker version only works to test web applications using selenium chromedriver.

First we need to use the Git Submodule that aims to TESTAR_api project, then use maven to build the api.

This can be done by the CI actions or by the user.

#### Dockerfile

Use existing DECODER version image from testartool Docker Hub

`FROM testartool/decoder-chromedriver`

Add built TESTAR api to the image

`ADD testar_api/target/testar-client-api-1.7.0.jar testar/bin`

Copy the runTESTARapi script (to execute the TESTAR api) inside the image

`COPY runTESTARapi /runTESTARapi`

`RUN chmod 777 /runTESTARapi`

Indicate that runTESTARapi script is the default Docker command to execute

`CMD [ "sh", "/runTESTARapi"]`

#### runTESTARapi

Enable X-Server framebuffer inside the Docker container

`echo "Start Xvfb"`

`/opt/bin/start-xvfb.sh &`

Execute TESTAR api to listen DECODER requests

`cd /testar/bin`

`java -jar testar-client-api-1.7.0.jar`

#### Build and Run Docker image

`docker build -t decoder/testar:latest . `

`docker run -d --name testar -p 8080:8080 decoder/testar:latest`


TESTAR Development branch
-------------------------

https://github.com/TESTARtool/TESTAR_dev/tree/decoder_master

https://github.com/TESTARtool/TESTAR_dev/tree/decoder_dev

TESTAR Github Docker Image Build
--------------------------------

https://github.com/TESTARtool/TESTAR_dev/tree/decoder_docker

TESTAR DECODER Chromedriver image
---------------------------------

https://hub.docker.com/r/testartool/decoder-chromedriver
