FROM testartool/decoder-chromedriver

ADD testar_api/target/testar-client-api-1.7.0.jar testar/bin

COPY runTESTARapi /runTESTARapi
RUN chmod 777 /runTESTARapi

CMD [ "sh", "/runTESTARapi"]